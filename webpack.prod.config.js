const merge = require('webpack-merge');
const common = require('./webpack.common.js');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = merge(common, {
  mode: "production",
  plugins: [new HtmlWebpackPlugin({
    template: 'public/index.html',
    favicon: 'public/favicon.ico',
    inject: false
  })],
});
